:- module show.
:- interface.
:- import_module char, int, list, maybe, string.
:- use_module io.

:- typeclass showable(T) where [
    func show(T) = string,
    mode show(in) = out is det
    % pred show(T, T),
    % mode show(di, uo) is det
].
:- instance showable(int).
:- instance showable(char).
:- instance showable(string).
:- instance showable(io.error).
:- instance showable(maybe(A)) <= showable(A).
:- instance showable({A, B}) <= (showable(A), showable(B)).
:- instance showable(list(A)) <= showable(A).
:- instance showable(io.maybe_partial_res(A)) <= showable(A).

:- implementation.

:- pragma terminates(show/1).

:- instance showable(int) where [
    (show(Int) = string.from_int(Int))
].
:- instance showable(char) where [
    show(C) = string.from_char(C)
].
:- instance showable(string) where [
    show(Str) = Str
].
:- instance showable(maybe(A)) <= showable(A) where [
    show(yes(X)) = "yes(" ++ show(X) ++ ")",
    show(no) = "no"
].
:- instance showable({A, B}) <= (showable(A), showable(B)) where [
    show({X, Y}) = "{" ++ show(X) ++ ", " ++ show(Y) ++ "}"
].
:- instance showable(list(A)) <= showable(A) where [
    show(Xs) = "[" ++ string.join_list(", ", list.map(show, Xs)) ++ "]"
].
:- instance showable(io.error) where [
    show(Err) = io.error_message(Err)
].
:- instance showable(io.maybe_partial_res(A)) <= showable(A) where [
    show(io.ok(X)) = show(X),
    show(io.error(X, Err)) =
        string.format(
            "Partial result\n%s\nwith error:\n%s",
            [s(show(X)), s(show(Err))])
].
