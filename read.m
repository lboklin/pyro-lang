:- module read.
:- interface.
:- import_module expr, show, string, list, maybe, parsing_utils.

% checked and reduced
:- type result
  ---> ok(indexed_expr, indexed_expr)
     ; free_variable(list({string, named_expr}), string, named_expr)
     ; hole(list({string, named_expr}), named_expr)
     ; type_error(type_env(indexed_expr), indexed_expr, string)
     ;
         parse_error(
             error_message :: maybe(string),
             error_line    :: int,
             error_col     :: int
         ).
:- instance showable(result).
:- instance showable(parse_result(E)) <= showable(E).

:- pred read_no_checkred(string::in, parse_result(named_expr)::out) is cc_multi.
:- pred read(string::in, result::out) is cc_multi.

:- func show_formatted(int, named_expr) = string.
:- func show_formatted(string, int, named_expr) = string.

:- implementation.

:- import_module nat, unit, int, bool, char, map, utils.
:- use_module check_reduce, exception, require, io, enum.

:- instance showable(result) where [
  show(ok(E, A)) = show(ok(E, A)),
  show(free_variable(_, V, E)) = "Free variable " ++ show(V) ++ " in expression:\n" ++ show(E),
  show(hole(_, E)) = "Found hole in expression (holes are not yet supported):\n" ++ show(E),
  show(type_error(Ctx, E, Desc)) = show(type_error(Ctx, E, Desc)),
  show(parse_error(MMsg, Line, Col)) =
    "Parse error at " ++ show(Line) ++ ":" ++ show(Col) ++ (if MMsg = yes(Msg) then "\n" ++ Msg else "")
].

:- instance showable(parse_result(E)) <= showable(E) where [
  show(parsing_utils.ok(E)) = show(E),
  show(parsing_utils.error(Msg, L, C)) = show(parse_error(Msg, L, C))
].

:- func from_yes(maybe(T)) = T is semidet.
from_yes(yes(X)) = X.
:- func from_yes_first({maybe(T1), T2}) = {T1, T2} is semidet.
from_yes_first({yes(X), Y}) = {X, Y}.

% read but don't check or reduce
read_no_checkred(String, Result) :-
    parsing_utils.parse(String, skip_ws, parse_all, Result).
% read, check and reduce
read(String, Res) :-
    read_no_checkred(String, Result),
    (
        Result = parsing_utils.ok(E),
        ResIE = to_de_bruijn([], E),
        (
            ResIE = ok(IE),
            ChkRedRes = check_reduce.check_reduce([], IE),
            (
                ChkRedRes = check_reduce.ok(E_, A),
                Res = ok(E_, A)
            ;
                ChkRedRes = check_reduce.type_error(Ctx, E_, Desc),
                % if I remove this, it never terminates for some reason :(
                require.unexpected($pred, "FIXME: return result gracefully for input:\n" ++ show(E) ++ "\nResult:\n" ++ show(ChkRedRes)),
                Res = type_error(Ctx, E_, Desc)
            )
        ;
            ResIE = free_variable(Ctx, V, E_),
            NamedCtx = list.filter_map(from_yes_first, Ctx),
            Res = free_variable(NamedCtx, V, E_)
        ;
            ResIE = hole(Ctx, E_),
            NamedCtx = list.filter_map(from_yes_first, Ctx),
            Res = hole(NamedCtx, E_)
        )
    ;
        Result = parsing_utils.error(Msg, L, C),
        Res = parse_error(Msg, L, C)
    ).

:- pred parse_all(src::in, named_expr::out, ps::in, ps::out) is semidet.
parse_all(Src, E) -->
    parse(Src, E),
    eof(Src, _).

:- pred parse(src::in, named_expr::out, ps::in, ps::out) is semidet.
parse(Src, E) -->
    skip_ws(Src, _),
    (
        keyword(id_chars, "let", Src, _) ->
            (
                ann_var(Src, Ann) ->
                    { {V, A} = Ann },
                    punct("=", Src, _),
                    parse(Src, Val)
            ;
                id(Src, Id),
                punct("=", Src, _),
                parse(Src, Val),
                {
                    V = v(Id),
                    A = var(wildcard)
                }
            ),
            (
                punct(";", Src, _) ->
                    parse(Src, Body),
                    { E = app(lambda(l(V, A), Body), Val) }
            ;
                current_offset(Src, Offset),
                fail_with_message(string.format(
                    "let-expression expected a separator (`;`) between the binding\n" ++
                    "let (%s : %s) = %s" ++
                    "\nand the expression that follows it.\n" ++
                    "Examples:\n" ++
                    "With type annotation and newline:\n" ++
                        "\tlet (x : T) = a;\n" ++
                        "\te\n" ++
                    "Elided type annotation and inline:\n" ++
                        "\tlet x = a; e",
                    [s(show(V)), s(show(A)), s(show(Val))]),
                    Offset, Src, E)
            )

    ;
        ann_var(Src, {V, A}) ->
            skip_ws(Src, _),
            (
                map_op(Src, _) -> parse(Src, E_), { E = lambda(l(V, A), E_) }
            ;
                arrow_op(Src, _) -> parse(Src, E_), { E = pi_type(l(V, A), E_) }
            ;
                current_offset(Src, Offset),
                fail_with_message(
                    "Variable annotations are only valid in the context of " ++
                    "λ-abstractions and Π-types.\n" ++
                    "Examples:\n" ++
                    "λ-expressions:\n" ++
                        "\t(x : a) \\-> f(x)\n" ++
                        "\t(x : a) ↦ f(x)\n" ++
                    "Π-type expressions:\n" ++
                        "\t(x : a) -> b(x)\n" ++
                        "\t(x : a) → b(x)\n",
                    Offset, Src, E
                )
            )

    ;
        id(Src, Id), skip_ws(Src, _), map_op(Src, _) ->
            parse(Src, E_), { E = lambda(l(v(Id), var(wildcard)), E_) }

    ;
        wildcard(Src, _), skip_ws(Src, _), map_op(Src, _) ->
            parse(Src, E_), { E = lambda(l(wildcard, var(wildcard)), E_) }

    ;
        atom(Src, A), skip_ws(Src, _), arrow_op(Src, _) ->
            parse(Src, B), { E = pi_type(l(wildcard, A), B) }

    ;
        atom(Src, Atom) ->
            { E = Atom }

    ;
        punct("(", Src, _),
        current_offset(Src, Offset),
        parse(Src, E_),
        punct(")", Src, _) ->
            fail_with_message("Redundant brackets around expression:\n" ++ show(E_), Offset, Src, E_),
            { E = E_ }

    ;
        { false }
    ),
    skip_ws(Src, _).

:- pred atom_or_brackets(src::in, named_expr::out, ps::in, ps::out) is semidet.
atom_or_brackets(Src, E) -->
    ( brackets("(", ")", parse, Src, E_) ->
        { E = E_ }
    ; atom(Src, E)
    ).

:- pred atom(src::in, named_expr::out, ps::in, ps::out) is semidet.
atom(Src, E) -->
    ( builtin(Src, Builtin) -> { E = Builtin }
    ; traditional_app(Src, App) -> { E = App }
    ; id(Src, Id) -> { E = var(v(Id)) }
    ; {false}
    ).

:- pred builtin(src::in, named_expr::out, ps::in, ps::out) is semidet.
builtin(Src, E) -->
    ( keyword(id_chars, "#B", Src, _) -> { E = bool_type }
    ; keyword(id_chars, "#t", Src, _) -> { E = literal(boolean(yes)) }
    ; keyword(id_chars, "#f", Src, _) -> { E = literal(boolean(no)) }
    ; sort(Src, S) -> { E = S }
    ; wildcard(Src, E)
    ).

:- pred wildcard(src::in, named_expr::out, ps::in, ps::out) is semidet.
wildcard(Src, E) --> keyword(id_chars, "_", Src, _), { E = var(wildcard) }.

:- pred ann_var(src::in, {var, named_expr}::out, ps::in, ps::out) is semidet.
ann_var(Src, {V, A}) -->
    skip_ws(Src, _),
    punct("(", Src, _),
    skip_ws(Src, _),
    ( if id(Src, Id)
    then {V = v(Id)}
    else if keyword(id_chars, "_", Src, _)
    then {V = wildcard}
    else {false}
    ),
    punct(":", Src, _),
    parse(Src, A),
    punct(")", Src, _).

:- pred arrow_op(src::in, unit::out, ps::in, ps::out) is semidet.
arrow_op(Src, unit) -->
    skip_ws(Src, _),
    % punct("→", Src, _) doesn't work..
    ( next_char(Src, C), { enum.to_int(C) = 0'→ } -> skip_ws(Src, _)
    ; punct("->", Src, _) -> {true}
    ; {false}
    ).

:- pred map_op(src::in, unit::out, ps::in, ps::out) is semidet.
map_op(Src, unit) -->
    skip_ws(Src, _),
    ( next_char(Src, C), { enum.to_int(C) = 0'↦ } -> skip_ws(Src, _)
    ; punct("\\->", Src, _) -> {true}
    ; {false}
    ).

:- pred sort(src::in, named_expr::out, ps::in, ps::out) is semidet.
sort(Src, sort(S)) -->
    skip_ws(Src, _),
    ( (
        next_char(Src, ('#')),
        next_char(Src, ('T')),
        next_char(Src, ('(')),
        int_literal(Src, I),
        punct(")", Src, _)
      ) ->
        { S = typ(enum.from_int(I)) }

    ; keyword(id_chars, "★", Src, _) -> { S = typ(zero) }
    ; keyword(id_chars, "◻", Src, _) -> { S = typ(succ(zero)) }
    ; next_char(Src, ('*')) ->
        ( next_char(Src, ('*')) -> { S = typ(succ(zero)) }
        ; { S = typ(zero) }
        )

    ; {false}
    ).

:- pred traditional_app(src::in, named_expr::out, ps::in, ps::out) is semidet.
traditional_app(Src, E) -->
    ( id(Src, Id) -> { F = var(v(Id)) }
    ; brackets("(", ")", parse, Src, E_) -> { F = E_ }
    ; {false}
    ),
    one_or_more(tuple, Src, Tups),
    {
        Args = list.foldl(func(Es, Acc) = Acc ++ Es, Tups, []),
        E = list.foldl(func(N, Acc) = app(Acc, N), Args, F)
    }.

:- pred tuple(src::in, list(named_expr)::out, ps::in, ps::out) is semidet.
tuple(Src, Es) -->
    next_char(Src, ('(')),
    separated_list(",", parse, Src, Es),
    skip_ws(Src, _),
    next_char(Src, (')')).

:- pred id(src::in, string::out, ps::in, ps::out) is semidet.
id(Src, Id) -->
    { IdNextChars = id_chars ++ "0123456789_'-" },
    ( if identifier(id_chars, IdNextChars, Src, Id_)
    then { Id = Id_ }
    else if identifier(id_chars ++ "#", IdNextChars, Src, S)
            % next_char_no_progress(Src, ('#')), non_whitespace(Src, S)
    then 
        current_offset(Src, Offset),
        { Id = S },
        fail_with_message("Unrecognised special symbol: " ++ S, Offset, Src, _:unit)
    else
        { Id = "", false }
    ).

:- func id_chars = string.
id_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".

:- pred comment(src::in, string::out, ps::in, ps::out) is semidet.
comment(Src, Cmt) -->
    current_offset(Src, Offset),
    (
        next_char(Src, ('#')), next_char(Src, ('(')) ->
            % FIXME: this isn't ideal because it matches brackets in comments,
            % which we actually don't care about.
            skip_until_closing_bracket(Src),
            current_offset(Src, To),
            { input_substring(Src, Offset, To, Cmt) }
    ;
        next_char(Src, ('#')), next_char_no_progress(Src, C), { char.is_whitespace(C) } ->
            skip_to_eol(Src, _),
            skip_ws(Src, _),
            current_offset(Src, To),
            { input_substring(Src, Offset, To, Cmt) }
    ;
        { false }
    ).

:- pred non_whitespace(src::in, string::out, ps::in, ps::out) is semidet.
non_whitespace(Src, String) -->
    next_char(Src, C),
    ( if { char.is_whitespace(C) }
    then
        { false }
    else
        ( if non_whitespace(Src, Rest)
        then { string.first_char(String, C, Rest) }
        else { String = string.from_char(C) }
        )
    ).

:- pred skip_until_closing_bracket(src::in, ps::in, ps::out) is semidet.
skip_until_closing_bracket(Src) -->
    skip_ws(Src, _),
    (
        next_char(Src, ('(')) ->
            skip_until_closing_bracket(Src),
            skip_until_closing_bracket(Src)

    ;
        next_char(Src, C) ->
            (
                { C = ('(') } -> skip_until_closing_bracket(Src)
            ;
                { C = (')') } -> { true }
            ;
                skip_until_closing_bracket(Src)
            )

    ;
        fail_with_message("Unmatched opening bracket", Src, _:unit)
    ).

% Consume any input up to EOF.
:- pred skip_to_eof(src::in, unit::out, ps::in, ps::out) is semidet.
skip_to_eof(Src, unit, !PS) :-
    ( if eof(Src, unit, !PS) then
        true
    else
        next_char(Src, _, !PS),
        disable_warning [suspicious_recursion] (
            skip_to_eof(Src, _, !PS)
        )
    ).

:- pred skip_ws(src::in, unit::out, ps::in, ps::out) is semidet.
skip_ws(Src, unit, !PS) :-
    ( if comment(Src, _, !PS)
    then true
    else if
        next_char(Src, C, !PS),
        char.is_whitespace(C)
    then
        disable_warning [suspicious_recursion] (
            skip_ws(Src, _, !PS)
        )
    else semidet_true
    ).


%%% Showing

% max col width
:- func max_line_width = int.
max_line_width = 30.
% spaces per indent
:- func indent_width = int.
indent_width = 4.

% Assumes already on a new line. Indents itself.
show_formatted(Ind, E) = show_formatted("", Ind, E).
show_formatted(_, Ind, E@literal(_)) = indt(Ind) ++ show(E).
show_formatted(_, Ind, E@var(_)) = indt(Ind) ++ show(E).
show_formatted(_, Ind, E@bool_type) = indt(Ind) ++ show(E).
show_formatted(_, Ind, E@sort(_)) = indt(Ind) ++ show(E).
show_formatted(Pad, Ind, lambda(l(V, A), Body)) = Str :-
    StrV = show(V),
    StrA = show(A),
    StrInd = Pad ++ indt(Ind),
    Line1 =
        ( if
            Line1_ = StrInd ++ bracket("(", ")", StrV ++ " : " ++ StrA) ++ " ↦\n",
            string.length(Line1_) < max_line_width
        then Line1_
        % else StrInd ++ StrV ++ "\n" ++ indt(Ind + 1) ++ ": " ++ show_formatted(Pad, Ind, A) ++ "\n" ++ StrInd ++ "↦"
        else StrInd ++ bracket("(", ")", StrV ++ " :\n" ++ show_formatted(Pad, Ind + 1, A)) ++ " ↦\n"
        ),
    Str = Line1 ++ show_formatted(Pad, Ind + 1, Body).
show_formatted(Pad, Ind, pi_type(l(V, A), Body)) = Str :-
    StrV = show(V),
    StrA = show(A),
    StrInd = Pad ++ indt(Ind),
    Line1 =
        ( if
            Line1_ = StrInd ++ bracket("(", ")", StrV ++ " : " ++ StrA) ++ " →\n",
            string.length(Line1_) < max_line_width
        then StrInd ++ Line1_
        else StrInd ++ bracket("(", ")", StrV ++ " :\n" ++ show_formatted(Pad, Ind + 1, A)) ++ " →\n"
        ),
    Str = Line1 ++ show_formatted(Pad, Ind + 1, Body).
show_formatted(Pad, Ind, app(M, N)) = Str :-
    StrInd = Pad ++ indt(Ind),
    Str0 = StrInd ++ show(app(M, N)),
    ( if string.length(Str0) < max_line_width
    then Str = Str0
    else
        StrM0 = show_assoc(M, show_formatted(Pad, Ind)),
        StrM =
            ( if string.append(StrM0_, "))", StrM0)
            then
                StrM0_ ++ ")\n" ++
                StrInd ++ ")"
            else StrM0
            ),
        Str =
            ( if StrN = show(N), string.length(StrN) < max_line_width
            then StrM ++ "(" ++ StrN ++ ")"
            else
                StrM ++ "(\n" ++
                    show_formatted(Pad, Ind + 1, N) ++ "\n" ++ indt(Ind) ++
                ")"
            )
    ).

% surround with brackets if it isn't an atom
:- func show_assoc(named_expr, func(named_expr) = string) = string.
show_assoc(E, F) = % bracket(Str0) :-
    ( if not is_atom(E)
    then bracket("(", ")", Str0) % "(" ++ Str0 ++ ")"
    else Str0
    ) :-
        Str0 = F(E).

:- func unbracket(string) = string.
unbracket(BStr) = Str :-
    ( if string.append("(", Rest, BStr), string.append(IStr, ")", Rest)
    then Str = unbracket(IStr)
    else Str = BStr
    ).

:- func bracket(string, string, string) = string.
bracket(Open, Close, Str0) =
    ( if string.first_char(Str0, (' '), Rest)
    then " " ++ bracket(Open, Close, Rest)
    else Open ++ Str0 ++ Close
    ).

% indentation string
:- func indt(int) = string.
indt(Ind) = string.duplicate_char((' '), Ind * indent_width).
