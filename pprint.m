:- module pprint.
:- interface.
:- import_module nat, string, int, bool.
:- use_module io.

:- type colour_mode
  ---> regular 
     ; bold 
     ; underline 
     ; high_intensity 
     ; boldhigh_intensity 
     ; background 
     ; high_intensity_background
     .
:- type colour
  ---> black
     ; white
     ; red
     ; green
     ; yellow
     ; blue
     ; purple
     ; cyan
     .
:- func colour(nat) = colour.
% :- type rgb ---> rgb(int, int, int).
% :- type fg ---> fg(rgb, list(attr)).
% :- type bg ---> bg(rgb).

% :- type attr ---> regular ; bold ; faint ; block ; underline.

:- pred print_coloured(string, io.io, io.io).
:- mode print_coloured(di, di, uo) is det.
:- mode print_coloured(in, di, uo) is det.

:- pred print_line_coloured(string, io.io, io.io).
:- mode print_line_coloured(di, di, uo) is det.
:- mode print_line_coloured(in, di, uo) is det.

:- func colour_string(string, colour_mode, nat) = string.
:- func colour(bool, int, int, int) = string is semidet.
:- func colour_det(bool, int, int, int) = string.
:- func colour_code(colour, colour_mode) = string.
:- func clear = string.

:- implementation.
:- import_module show, list.
:- use_module require.

print_coloured(Str, !IO) :-
    io.call_system("echo -e \"" ++ Str ++ "\"", _, !IO).

print_line_coloured(Str, !IO) :-
    io.call_system("echo -e \"" ++ Str ++ "\"", _, !IO),
    io.nl(!IO).

colour_string(Str, Mode, N) =
    colour_code(colour(N), Mode) ++
    Str ++
    clear.

colour(FG, R, G, B) =
    string.format(
        "\\e[%s;2;%s;%s;%sm]",
        [s(show(FG_BG)), s(show(R)), s(show(G)), s(show(B))]
    ) :-
        FG_BG = (if FG = yes then 38 else 48),
        0 =< R, R < 255,
        0 =< G, G < 255,
        0 =< B, B < 255.

colour_det(FG, R, G, B) =
    ( if Clr = colour(FG, R, G, B)
    then Clr
    else require.unexpected($pred, "colour out of range")
    ).

colour(zero) = green.
colour(succ(zero)) = yellow.
colour(succ(succ(zero))) = red.
colour(succ(succ(succ(zero)))) = purple.
colour(succ(succ(succ(succ(zero))))) = blue.
% colour(succ(succ(succ(succ(succ(zero)))))) = cyan.
% cycle around
colour(succ(succ(succ(succ(succ(N)))))) = colour(N).


% text reset
clear = "\\e[0m".

% regular
colour_code(black, regular)                    = "\\e[0;30m".
colour_code(red, regular)                      = "\\e[0;31m".
colour_code(green, regular)                    = "\\e[0;32m".
colour_code(yellow, regular)                   = "\\e[0;33m".
colour_code(blue, regular)                     = "\\e[0;34m".
colour_code(purple, regular)                   = "\\e[0;35m".
colour_code(cyan, regular)                     = "\\e[0;36m".
colour_code(white, regular)                    = "\\e[0;37m".

% bold
colour_code(black, bold)                       = "\\e[1;30m".
colour_code(red, bold)                         = "\\e[1;31m".
colour_code(green, bold)                       = "\\e[1;32m".
colour_code(yellow, bold)                      = "\\e[1;33m".
colour_code(blue, bold)                        = "\\e[1;34m".
colour_code(purple, bold)                      = "\\e[1;35m".
colour_code(cyan, bold)                        = "\\e[1;36m".
colour_code(white, bold)                       = "\\e[1;37m".

% underline
colour_code(black, underline)                  = "\\e[4;30m".
colour_code(red, underline)                    = "\\e[4;31m".
colour_code(green, underline)                  = "\\e[4;32m".
colour_code(yellow, underline)                 = "\\e[4;33m".
colour_code(blue, underline)                   = "\\e[4;34m".
colour_code(purple, underline)                 = "\\e[4;35m".
colour_code(cyan, underline)                   = "\\e[4;36m".
colour_code(white, underline)                  = "\\e[4;37m".

% high_intensity
colour_code(black, high_intensity)             = "\\e[0;90m".
colour_code(red, high_intensity)               = "\\e[0;91m".
colour_code(green, high_intensity)             = "\\e[0;92m".
colour_code(yellow, high_intensity)            = "\\e[0;93m".
colour_code(blue, high_intensity)              = "\\e[0;94m".
colour_code(purple, high_intensity)            = "\\e[0;95m".
colour_code(cyan, high_intensity)              = "\\e[0;96m".
colour_code(white, high_intensity)             = "\\e[0;97m".

% boldhigh_intensity
colour_code(black, boldhigh_intensity)         = "\\e[1;90m".
colour_code(red, boldhigh_intensity)           = "\\e[1;91m".
colour_code(green, boldhigh_intensity)         = "\\e[1;92m".
colour_code(yellow, boldhigh_intensity)        = "\\e[1;93m".
colour_code(blue, boldhigh_intensity)          = "\\e[1;94m".
colour_code(purple, boldhigh_intensity)        = "\\e[1;95m".
colour_code(cyan, boldhigh_intensity)          = "\\e[1;96m".
colour_code(white, boldhigh_intensity)         = "\\e[1;97m".

% background
colour_code(black, background)                 = "\\e[40m".
colour_code(red, background)                   = "\\e[41m".
colour_code(green, background)                 = "\\e[42m".
colour_code(yellow, background)                = "\\e[43m".
colour_code(blue, background)                  = "\\e[44m".
colour_code(purple, background)                = "\\e[45m".
colour_code(cyan, background)                  = "\\e[46m".
colour_code(white, background)                 = "\\e[47m".

% high_intensity_background
colour_code(black, high_intensity_background)  = "\\e[0;100m".
colour_code(red, high_intensity_background)    = "\\e[0;101m".
colour_code(green, high_intensity_background)  = "\\e[0;102m".
colour_code(yellow, high_intensity_background) = "\\e[0;103m".
colour_code(blue, high_intensity_background)   = "\\e[0;104m".
colour_code(purple, high_intensity_background) = "\\e[0;105m".
colour_code(cyan, high_intensity_background)   = "\\e[0;106m".
colour_code(white, high_intensity_background)  = "\\e[0;107m".
