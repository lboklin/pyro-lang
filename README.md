## Why Pyro?

Pyro's two core philosophies are
1. Expressive minimalism through an expressive type system presented using a minimal and consistent syntax, where expressions look and behave consistently regardless of their context.
2. Organic Programming: inline annotations and row-types keeps your code sound and safe without distractions like defining rigid or overly generalised top-level declarations - allow your types to grow until it's time for harvest (through introduction and elimination of product and sum types).

The syntax aims to be extremely simple and with as few special rules as possible, making it easier to learn and more uniform (while aiming to remain readable) - you should never have to consult the language reference after spending a few minutes of familiarising yourself with it. The rules should all fit in your working memory.

The language has no concept of a 'top-level'. A file is a single expression. A 'project' would be a single expression. Types are expressions. Let bindings are syntactic sugar for an expression where the subsequent expression forms the body of a lambda expression that is applied to the bound expression. With row types, you have first class modules being mere record expressions from which you retrieve its labels just like using imported functions in any other language with modules.

The type system is classified as an Injective PTS (Pure Type System). It's decidable (always terminates with a type or descriptive type error) and syntax directed (any well-typed expression can have exactly one type induced from its lexically scoped context). Types are also values which may be parameterised over other values.

_Warning: Don't use this language for anything important. It's a toy language developed by a single individual with a special interest._

## Requirements

You need `mmc` (the Mercury compiler) and (optionally) `hvm` (the HVM compiler).

With Nix you should be able to simply run `nix shell nixpkgs#mercury github:Kindelia/HVM` to put both in your shell environment.

Without Nix you should be able to install Mercury from your package manager or whatever. HVM is more recent so you'll probably have to clone the repo, build it and then (optionally) add the executable to your path: `export PATH=<path/to/hvm>:$PATH`. The included scripts assume `mmc` and `hvm` are in your path.

Links:
- Mercury: https://mercurylang.org/
- HVM: https://github.com/Kindelia/HVM
- Nix: https://nixos.org/ (how to get: https://nixos.org/download.html)

## How to build

1. With `mmc` (the Mercury compiler) in your path, run `mmc --make pyro`.
2. If you can make yourself a cup of tea or coffee in under five seconds (maybe a few more depending on your CPU), feel free to do so as you wait.

## How to compile to HVM

Run `./pyro compile` with the path to a `.pr` source file as argument and it will place an HVM source file right next to it.

## How to evaluate an expression

After building `pyro`, you can either
- run `./pyro eval` with an expression,
- run `./pyro compile` with a filename and then either
  - run `hvm r` with the filename
  - execute binary after [compiling](https://github.com/Kindelia/HVM/blob/master/README.md) the HVM source
- run `./pyro repl` for an evaluation prompt, though type errors will actually crash it right now because there's a bug that requires throwing an exception to avoid it hanging.

## Ok but what does it even look like?

The syntax is still in flux, because the goal is to find the best balance between readability, ergonomics and elegance (through simplicity), with special emphasis on simplicity. In the end, however, the language is not its syntax: nothing prevents alternative representations as long as they're all isomorphic to each other. That said, the following describes the currently supported syntax.

### TL;DR

The basic syntax is
- function applications: `f(x)`
- functions: `(x : T) ↦ f(x)`
- dependent function/arrow types: `(x : A) → B(x)`
- a few builtin symbols (for now; these may change in the future) - note that they all begin with `#` to make them stand out from other symbols
  - `#T`, the type of types that must be applied to a level represented by a natural number literal, like so: `#T(0)`
  - `#B`, the boolean type
  - boolean literals `#t` and `#f`
  - `# `, comment until EOL
  - `#(`, comment until matching `)` (yes, that means brackets inside the comment must be matched, which could constitute a problem to solve)

Those rules form the entirety of the underlying language, but there are some syntactic sugar and conveniences:
- ASCII lambda operator: `\->` <=> `↦`
- ASCII arrow operator: `->` <=> `→`
- ASCII type of types: `*` <=> `★` <=> `#T(0)`
- ASCII type of `★`: `**` <=> `◻` <=> `#T(1)`
- multiple sequential applications: `f(x, y, z)` <=> `f(x)(y)(z)` <=> `f(x)(y,z)` etc
- eliding a function's domain type: `x ↦ f(x)` ->> `(x : _) ↦ f(x)`
- non-dependent functions can omit the binding: `A → B` ->> `(_ : A) → B`
- let binding is function application: `let (v : T) = E; ...` ->> `((v : T) ↦ ...)(E)`
- let binding with elided type: `let v = E; ...` ->> `(v ↦ ...)(E)`

Both the lambda and arrow operators associate to the right, so `a → b → c` would be the same as `a → (b → c)`, and similarly for (`↦`). `:` reads everything to its right side as one expression, so you would read `(f : a → b → c)` as `(f : (a → b → c))`. However, the parser is very pedantic and will refuse redundant or omitted brackets. This is because brackets are supposed to use to disambiguate and there's no ambiguity in the syntax to disambiguate. Inconsistent use of brackets only serve to instill doubt and confusion as to the rules of associativity.

### Examples

The identity function polymorphic over types in `★` (the type of types):
```
(A : ★) ↦ (x : A) ↦ x
```

The identity function applied to the builtin boolean type `#B` to form the identity functions over booleans:
```
((A : ★) ↦ (x : A) ↦ x)(#B)

```

The identity function over booleans applied to the builtin truth literal `#t` of type `#B` using let-bindings:
```
let id = (A : ★) ↦ (x : A) ↦ x;
# Everything below has `id` in scope and this is a comment
let id-bool = id(#B);
#(this is a block comment) id-bool(#t)
```
which evaluates to `#t` with `#t : #B` as the derived type judgement.

See some more half-baked examples in ./tests/.

### Limitations

- Wildcards are only marginally useful at the moment as functions must be immediately typed. Clever type inference is something that may be looked into in the future.
- There is no recursion. The language is not Turing-complete (on purpose). The future goal is to facilitate Turing-complete programming through _explicit_ syntax.
- Row types are not yet implemented

## Things left to do

- [ ] Solidify the syntax
- [x] Explain the syntax
- [ ] Implement extensible row types and their syntax
- [x] Eliminate the requirement for type annotation in let bindings (since we could theoretically determine the type from the expression)
- [ ] Make `pyro compile` take target language as argument (default to pyro->HVM->C->binary?)
- [ ] Implement a filepath builtin type and filepath literals to allow using expressions from other files inline (while preventing circular imports)
- [ ] Make a really cool structural editor?
- [ ] Look into portability improvements (such as not employing the massive hack currently that is using `echo` for printing coloured expressions)

... and plenty more, probably.

## References and inspirational sources

- The row types in [roc-lang](https://roc-lang.org)
- Effect handling in [Koka-lang](https://koka-lang.github.io/koka/doc/index.html)
- "Abstracting Extensible Data Types" by J. Garret Morris & James McKinna, 2019
- "Pure Type Systems for Functional Programming" by Jan-Willem Roorda & Johan Jeuring, 2000
- [Agda](https://agda.readthedocs.io) - cool type system stuff like Cubical Type Theory that may or may not become relevant
- Filepath builtin literals and attribute sets as modules in Nix

## Contact/community

Feel free to join [#pyro-lang:matrix.org](https://matrix.to/#/%23pyro-lang%3Amatrix.org) on Matrix if you have any questions, feedback or ideas.
